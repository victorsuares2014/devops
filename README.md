Claro, aqui está o texto corrigido:

## DevOps

# Francisco Victor Soares de Lima

Primeiro passo: utilizar os comandos

`terraform init`

<img src="./images/init.png">

para inicializar o Terraform, e logo em seguida utilizar

`terraform plan`
<img src="./images/plan.png">

para que o Terraform faça as verificações e nos mostre quais serão as alterações que serão realizadas.

Logo em seguida, podemos utilizar
`terraform apply`
<img src="./images/apply.png">

para executar o script e criar nosso servidor EC2 na AWS.
Se tudo correu bem, veremos no console da AWS isso:
<img src="./images/aws.png">

Devemos também configurar nosso pipeline do GitLab

através do script localizado neste projeto em `.gitlab-ci.yml`.

Na plataforma GitLab, precisamos configurar nossas variáveis de ambiente para darmos acesso à nossa conta do Docker Hub, para que possamos fazer o push de nossas imagens.
<img src="./images/var.png">

Devemos também observar se nosso runner está ativo:
<img src="./images/runner.png">

Após dar push no projeto, devemos ver se os stages ocorreram com sucesso:
<img src="./images/sucess.png">

Agora é só conferirmos nosso endereço público do servidor na AWS:
<img src="./images/helloworld.png">

e nossa imagem no Docker Hub:
<img src="./images/hub.png">
